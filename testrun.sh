#!/usr/bin/env bash
PORT=8083
gunicorn -w 4 --threads 2 -k sync -b 0.0.0.0:$PORT -t 30 gpsmon.app:app
