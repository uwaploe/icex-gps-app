/**
 * .. module:: dmm
 *   :synopsis: represent angles as degrees and decimal minutes.
 */
var Dmm = function(deg) {
    if(deg == null) {
        this.deg = 0;
        this.min = 0.;
    } else {
        if(deg < 0)
            this.deg = Math.ceil(deg);
        else
            this.deg = Math.floor(deg);

        this.min = (deg - this.deg)*60.;
    }
};

Dmm.prototype = {
    tostring: function() {
        var deg = Number(this.deg);
        var min = Number(Math.abs(this.min));
        var padding = (min < 10.) ? "0" : "";
        return deg.toFixed(0) + "-" + padding + min.toFixed(4);
    },
    todegrees: function() {
        return this.deg + this.min/60.;
    },
    isneg: function() {
        return (this.deg < 0);
    },
    abs: function() {
        var d = new Dmm(0);
        d.deg = Math.abs(this.deg);
        d.min = Math.abs(this.min);
        return d;
    }
};

// Parse a string representation of a Dmm object
function parseDmm(str) {
    var re = /([0-9]*)-([0-9\.]*)([NSEW])/i;
    var m = re.exec(str);
    var dmm = new Dmm();

    if(m == null)
        throw "Invalid input: " + str;

    dmm.deg = parseFloat(m[1]);
    dmm.min = parseFloat(m[2]);
    if(m[3].toUpperCase() == "S" || m[3].toUpperCase() == "W") {
        dmm.deg = -dmm.deg;
        dmm.min = -dmm.min;
    }

    return dmm;
}
