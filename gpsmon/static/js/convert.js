// Javascript functions for the coordinate-conversion web-app

function isoformat(d) {
    var tformat = "YYYY-MM-DD HH:mm:ss";
    return moment(d).utc().format(tformat) + "Z";
}

function show_error(msg) {
    Messenger().post({
        message: msg,
        type: "error",
        theme: "block",
        showCloseButton: true
    });
}

// Call server interface to convert X-Y to Lat-Lon
function tolatlon (x, y, url) {
    $.post(url, {"x": x, "y": y},
        function(data) {
            var lat = new Dmm(data.latitude/1.0e7);
            var lon = new Dmm(data.longitude/1.0e7);
            var hlat = "N", hlon = "E";

            if(lat.isneg()) {
                lat = lat.abs();
                hlat = "S";
            }

            if(lon.isneg()) {
                lon = lon.abs();
                hlon = "W";
            }

            $("#lat").val(lat.tostring() + hlat);
            $("#lon").val(lon.tostring() + hlon);
        },
        "json"
    );
}

// Call server interface to convert Lat-Lon to X-Y
function toxy (slat, slon, name, desc, url) {
    var lat, lon;

    try {
        lat = parseDmm(slat);
    } catch (x) {
        show_error("Invalid latitude format: " + slat);
    }

    try {
        lon = parseDmm(slon);
    } catch (x) {
        show_error("Invalid longitude format: " + slon);
    }

    $.post(url,
           {
               "lat": lat.todegrees(),
               "lon": lon.todegrees(),
               "name": name,
               "desc": desc
           },
           function(data) {
               var x = Number(data.x/1.0e3);
               var y = Number(data.y/1.0e3);

               $("#x").val(x.toFixed(1));
               $("#y").val(y.toFixed(1));
               $("#name").val("");
               $("#desc").val("");
           },
           "json"
    );
}

// Check the current Tracking Range grid orientation
function check_status(dataurl, timeout) {
    var source = new EventSource(dataurl);
    var task_id = null;
    var normal_color = $("#timestamp").css("color");
    var warning = function() {
        var $elem = $("#timestamp");
        $elem.css({"color": "red"});
    };

    if(timeout > 0) {
        task_id = setTimeout(warning, timeout*1000);
    }

    source.addEventListener("update", function(e){
        var data = JSON.parse(e.data);
        var lat = new Dmm(data.origin.latitude/1.0e7);
        var lon = new Dmm(data.origin.longitude/1.0e7);
        var hlat = "N", hlon = "E";

        if(task_id) {
            clearTimeout(task_id);
            $("#timestamp").css({"color": normal_color});
            task_id = setTimeout(warning, timeout*1000);
        }

        if(lat.isneg()) {
            lat = lat.abs();
            hlat = "S";
        }

        if(lon.isneg()) {
            lon = lon.abs();
            hlon = "W";
        }
        var az = Number(data.azimuth/1.0e3);
        var tstamp = new Date(Number(data.tsec)*1000);

        $("#lat0").text(lat.tostring()+hlat);
        $("#lon0").text(lon.tostring()+hlon);
        $("#heading").text(az.toFixed(1));
        $("#timestamp").text(isoformat(tstamp));

        $(".update").trigger("update");
    });

    source.addEventListener("gps_update", function(e){
        var data = JSON.parse(e.data);
        var dec = Number(data.declination);

        $("#declination").text(dec.toFixed(1));
    });

    source.addEventListener("ranges", function(e){
        var data = JSON.parse(e.data);
        try {
            $.each(data.refs, function(idx, obj) {
                var sel = '#' + obj.name + '-range';
                $(sel).text(Number(obj.range).toFixed(1) + ' m');
                sel = '#' + obj.name + '-bearing';
                $(sel).text(Number(obj.bearing).toFixed(1) + '°T');
            });
        } catch(x) {
            ;
        }
    });

    source.addEventListener("alert", function(e){
        var data = JSON.parse(e.data);
        var tstamp = new Date(data.timestamp*1000);
        show_error("["+isoformat(tstamp)+"] " + data.text);
    });
}

// Page initialization function. Installs the various event handlers
function initialize(urls) {

    $("#inverse").click(function(e) {
        e.preventDefault();
        tolatlon($("#x").attr("value"),
                 $("#y").attr("value"),
                 urls.latlon);
    });

    $("#x_y").bind("update", function(e) {
        tolatlon($("#x").attr("value"),
                 $("#y").attr("value"),
                 urls.latlon);
    });

    $("#forward").click(function(e) {
        e.preventDefault();
        toxy($("#lat").attr("value"),
             $("#lon").attr("value"),
             $("#name").attr("value"),
             $("#desc").attr("value"),
             urls.xy);
    });

    $("#lat_lon").bind("update", function(e) {
        toxy($("#lat").attr("value"),
             $("#lon").attr("value"),
             "",
             "",
             urls.xy);
    });

    $("#xylock").change(function(e) {
        var name = this.value;
        var $elem = $("#"+name);

        if(this.checked) {
            $elem.removeClass("unlocked");
            $elem.addClass("locked update");
            $("#control button").attr("disabled", "disabled");
            $("#lllock").attr("disabled", "disabled");
        } else {
            $elem.removeClass("locked update");
            $elem.addClass("unlocked");
            $("#control button").removeAttr("disabled");
            $("#lllock").removeAttr("disabled");
        }
    });

    $("#lllock").change(function(e) {
        var name = this.value;
        var $elem = $("#"+name);

        if(this.checked) {
            $elem.removeClass("unlocked");
            $elem.addClass("locked update");
            $("#control button").attr("disabled", "disabled");
            $("#xylock").attr("disabled", "disabled");
        } else {
            $elem.removeClass("locked update");
            $elem.addClass("unlocked");
            $("#control button").removeAttr("disabled");
            $("#xylock").removeAttr("disabled");
        }
    });

    check_status(urls.data, 60);
}
