#
#
import math
from decimal import Decimal


class Dmm(object):
    """
    Class to represent angles as degrees and decimal minutes

    >>> Dmm(30.5)
    30-30.0000
    >>> Dmm(-0.5)
    -0-30.0000
    >>> float(Dmm(-0.5))
    -0.5
    >>> abs(Dmm(-0.5))
    0-30.0000
    """
    deg_exp = Decimal('1')
    min_exp = Decimal('0.0001')

    def __init__(self, deg=0):
        self.deg = deg
        if self.deg < 0.:
            self.deg = math.ceil(self.deg)
        elif self.deg > 0.:
            self.deg = math.floor(self.deg)
        self.min = (deg - self.deg) * 60.

    def __str__(self):
        d = Decimal(str(self.deg))
        m = Decimal(str(abs(self.min)))
        padding = (m < 10.) and '0' or ''
        return '%s-%s%s' % (d.quantize(self.deg_exp),
                            padding, m.quantize(self.min_exp))
    __repr__ = __str__

    def __float__(self):
        return self.deg + self.min / 60.

    def is_neg(self):
        return float(self) < 0.

    def __abs__(self):
        d = Dmm()
        d.deg = abs(self.deg)
        d.min = abs(self.min)
        return d
