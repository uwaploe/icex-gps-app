#!/usr/bin/env python
#
# Flask web-app for ICEX GPS calculations
#
from flask import Flask, g, request, Response,\
    render_template, abort, make_response
import redis
import time
import geomag as gm
import simplejson as json
from io import StringIO
from zipfile import ZipFile, ZIP_DEFLATED
import grpc
from gics import api_pb2, api_pb2_grpc
from google.protobuf import json_format
from .utils import Dmm


# Icons for KML output, must be located in static/icons
ICONS = ['origin.png', 'rfgps.png', 'target.png']

app = Flask(__name__)
# app.wsgi_app = ReverseProxied(app.wsgi_app)
app.config.from_object(__name__)
app.config.update(dict(
    REDIS_HOST='localhost',
    REDIS_PORT=6379,
    REDIS_DB=0,
    DEBUG=False,
    SURVEYOR='127.0.0.1:10001',
    MAPPER='127.0.0.1:10000'
))
app.config.from_envvar('GPSMON_SETTINGS', silent=True)

# Map gRPC status codes to HTTP
http_codes = {
    grpc.StatusCode.INTERNAL: 400,
    grpc.StatusCode.UNAUTHENTICATED: 401,
    grpc.StatusCode.PERMISSION_DENIED: 403,
    grpc.StatusCode.UNIMPLEMENTED: 404,
    grpc.StatusCode.UNAVAILABLE: 503
}


def create_kmz(kml):
    """
    Pack a KML document along with icons into a KMZ archive and
    return the contents as a string.
    """
    zfile = StringIO()
    with ZipFile(zfile, 'w', ZIP_DEFLATED) as z:
        for icon in app.config.get('ICONS', []):
            with app.open_resource('static/icons/' + icon) as f:
                z.writestr('icons/' + icon, f.read())
        z.writestr('doc.kml', kml)
    contents = zfile.getvalue()
    zfile.close()
    return contents


def message_to_json(obj):
    """
    Serialize a protobuf message object to JSON without the indentation.
    """
    s = json_format.MessageToJson(obj)
    obj = json.loads(s)
    # MessageToJson writes int64 values as strings (??!!). The following
    # code is a work-around for timestamps
    if 'tsec' in obj:
        obj['tsec'] = int(obj['tsec'])
    return json.dumps(obj)


def connect_redis():
    """
    :rtype: redis.StrictRedis
    """
    return redis.StrictRedis(host=app.config['REDIS_HOST'],
                             port=app.config['REDIS_PORT'],
                             db=app.config['REDIS_DB'])


def connect_grpc():
    stubs = {}
    chan = grpc.insecure_channel(app.config['MAPPER'])
    stubs['mapper'] = api_pb2_grpc.MapperStub(chan)
    chan = grpc.insecure_channel(app.config['SURVEYOR'])

    stubs['surveyor'] = api_pb2_grpc.SurveyorStub(chan)
    return stubs


def event_stream(rd):
    assert isinstance(rd, redis.StrictRedis)
    pubsub = rd.pubsub()
    pubsub.subscribe('grid.alert', 'data.grid', 'data.refs',
                     'data.offsets')
    pset = api_pb2.PointSet()
    rm = api_pb2.RangeMatrix()
    for msg in pubsub.listen():
        if msg['type'] == 'message':
            if msg['channel'] == b'grid.alert':
                yield 'event: alert\ndata: {0}\n\n'.format(
                    msg['data'].decode('utf-8'))
            elif msg['channel'] == b'data.grid':
                yield 'event: update\ndata: {0}\n\n'.format(
                    msg['data'].decode('utf-8'))
            elif msg['channel'] == b'data.refs':
                json_format.Parse(msg['data'], pset)
                p = pset.points['origin']
                data = dict(latitude=p.latitude, longitude=p.longitude,
                            tsec=p.tsec)
                data['declination'] = gm.declination(float(p.latitude/1.0e7),
                                                     float(p.longitude/1.0e7))
                yield 'event: gps_update\ndata: {0}\n\n'.format(
                    json.dumps(data))
            elif msg['channel'] == b'data.offsets':
                json_format.Parse(msg['data'], rm)
                points = []
                n = rm.n
                for name, rng, brg in zip(rm.ids[1:n], rm.ranges[1:n],
                                          rm.azimuths[1:n]):
                    points.append({'name': name,
                                   'range': rng/1.0e3,
                                   'bearing': brg/1.0e3})
                yield 'event: ranges\ndata: {0}\n\n'.format(
                    json.dumps({'refs': points}))


@app.before_request
def before_request():
    g.rd = connect_redis()
    g.stub = connect_grpc()


@app.teardown_request
def teardown_request(exception):
    pass


@app.route('/_xy', methods=['POST'])
def toxy():
    p = api_pb2.Point()
    try:
        p.latitude = int(float(request.form['lat']) * 1.0e7)
        p.longitude = int(float(request.form['lon']) * 1.0e7)
        p.tsec = int(time.time())
        p.id = request.form.get('name')
    except ValueError:
        abort(404)

    try:
        gp = g.stub['mapper'].ToGrid(p)
    except grpc.RpcError as e:
        abort(http_codes.get(e.code(), 400))
    if gp.id:
        poi = dict(timestamp=gp.tsec,
                   x=round(gp.x/1.0e3, 1),
                   y=round(gp.y/1.0e3, 1),
                   name=gp.id,
                   desc=request.form.get('desc'))
        g.rd.publish('poi', json.dumps(poi))
    out = message_to_json(gp)
    resp = make_response(out)
    resp.headers['Content-Type'] = 'application/json'
    return resp


@app.route('/_latlon', methods=['POST'])
def tolatlon():
    gp = api_pb2.GridPoint()
    try:
        gp.x = int(float(request.form['x']) * 1.0e3)
        gp.y = int(float(request.form['y']) * 1.0e3)
    except ValueError:
        abort(404)
    try:
        p = g.stub['mapper'].ToGeo(gp)
    except grpc.RpcError as e:
        abort(http_codes.get(e.code(), 400))
    out = message_to_json(p)
    resp = make_response(out)
    resp.headers['Content-Type'] = 'application/json'
    return resp


@app.route('/stream')
def stream():
    return Response(event_stream(g.rd), mimetype='text/event-stream')


@app.route('/')
def index():
    data = {}
    try:
        p = g.stub['surveyor'].LookupPoint(api_pb2.PointReq(id="origin"))
    except grpc.RpcError:
        pass
    else:
        lat = Dmm(float(p.latitude/1.0e7))
        lon = Dmm(float(p.longitude/1.0e7))
        hlat = lat.is_neg() and 'S' or 'N'
        hlon = lon.is_neg() and 'W' or 'E'
        data['lat'] = '{0}{1}'.format(str(abs(lat)), hlat)
        data['lon'] = '{0}{1}'.format(str(abs(lon)), hlon)
        data['timestamp'] = time.strftime('%Y-%m-%d %H:%M:%SZ',
                                          time.gmtime(p.tsec))
        data['declination'] = '{:.1f}'.format(gm.declination(
            float(p.latitude/1.0e7),
            float(p.longitude/1.0e7)))
    return render_template('site.html', **data)


def build_kml(pset, data=None, kmz=False):
    """
    Helper function to create a KML document with the current location
    of the Camp Origin, the RF-GPS units, and (optionally) one or more
    targets.
    """
    def make_timestamp(t):
        return time.strftime(
            '%Y-%m-%dT%H:%M:%S+00:00', time.gmtime(t))
    points = []
    t = pset.points['origin'].tsec
    origin = dict(name='Origin',
                  desc='Tracking range origin',
                  lat=pset.points['origin'].latitude/1.0e7,
                  lon=pset.points['origin'].longitude/1.0e7)

    for name, p in pset.points.items():
        if name != 'origin':
            pt = dict(name=name,
                      desc='Hydrophone GPS beacon',
                      lat=p.latitude/1.0e7,
                      lon=p.longitude/1.0e7)
            gp = g.stub['mapper'].ToGrid(p)
            pt['x'] = round(gp.x/1.0e3, 1)
            pt['y'] = round(gp.y/1.0e3, 1)
            points.append(pt)

    if data:
        targets = []
        for target in data['targets']:
            p = g.stub['mapper'].ToGeo(api_pb2.GridPoint(
                x=int(target['x']*1.0e3),
                y=int(target['y']*1.0e3)))
            targets.append(dict(name=target['name'],
                                x=round(target['x'], 1),
                                y=round(target['y'], 1),
                                lat=p.latitude/1.0e7,
                                lon=p.longitude/1.0e7))

    return render_template('range.kml',
                           points=points,
                           origin=origin,
                           azimuth=0,
                           targets=targets,
                           kmz=kmz,
                           timestamp=make_timestamp(t))


@app.route('/kml', methods=['GET', 'PUT'])
def kml():
    try:
        pset = g.stub['surveyor'].AllPoints(api_pb2.Empty())
    except grpc.RpcError as e:
        abort(http_codes.get(e.code(), 400))
    kml = build_kml(pset,
                    data=request.get_json(force=True, silent=True))
    resp = make_response(kml)
    resp.headers['Content-Type'] = 'application/vnd.google-earth.kml+xml'
    return resp


@app.route('/kmz', methods=['GET', 'PUT'])
def kmz():
    try:
        pset = g.stub['surveyor'].AllPoints(api_pb2.Empty())
    except grpc.RpcError as e:
        abort(http_codes.get(e.code(), 400))
    kml = build_kml(pset,
                    data=request.get_json(force=True, silent=True),
                    kmz=True)
    resp = make_response(create_kmz(kml))
    resp.headers['Content-Type'] = 'application/vnd.google-earth.kmz'
    return resp
