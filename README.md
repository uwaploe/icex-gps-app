# ICEX Grid Monitor App

This repository contains a [Flask](http://flask.pocoo.org) based web
application which provides the user-interface for converting between ICEX
grid and geodetic coordinates.
